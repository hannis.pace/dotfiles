#! /bin/bash

# https://riseup.net/ru/security/message-security/openpgp/gpg-best-practices

sudo apt update -y
sudo apt install -y parcimonie

echo "UID (e.g. email)"
read uid

fp=`gpg --quick-gen-key $uid ed25519 cert 2d | tail -n 3 | head -n 1 | xargs`
#gpg --quick-add-key $fp cv25519 encr 1d
gpg --quick-add-key $fp ed25519 sign none
#gpg --quick-add-key $fp ed25519 auth 1d
#gpg --quick-add-key $fp rsa4096 encr 1d
gpg --output $fp.revoke.asc --gen-revoke $fp

gpg --export --armor $fp > $fp.pub.asc
gpg --export-secret-keys --armor $fp > $fp.priv.asc
gpg --export-secret-subkeys --armor $fp > $fp.sub_priv.asc
gpg --yes --delete-secret-key $fp
gpg --import $fp.sub_priv.asc

# run when offline, AFTER master key is saved offline
# gpg --send-keys $fp

git config --global user.signingkey $fp
git config --global commit.gpgsign true

echo "DONE! Remember to save *asc files and revoke cert offline"
