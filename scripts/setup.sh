#! /bin/bash

# you shouldn't execute this
exit
email="hannis.pace@protonmail.com"
name="Hannis Pace"

# connect to VPN

# set up ProtonVPN
# https://protonvpn.com/support/linux-vpn-tool/
sudo apt install -y openvpn dialog python3-pip python3-setuptools python3-wheel
sudo pip3 install protonvpn-cli
# enable ProtonVPN DNS Leak Protection and Kill Switch

# Create ProtonMail account
# https://mail.protonmail.com/create/new
# Enable 2FA: https://mail.protonmail.com/security


# install Tor Browser
# https://www.torproject.org/download/
# verify signature
# https://support.torproject.org/en/tbb/how-to-verify-signature/
gpg --auto-key-locate nodefault,wkd --locate-keys torbrowser@torproject.org
gpg --output ./tor.keyring --export 0xEF6E286DDA85EA2A4BA7DE684E2C6E8793298290
gpgv --keyring ./tor.keyring ~/Downloads/tor-browser.tar.xz.asc ~/Downloads/tor-browser.tar.xz

# run tor-browser

# create ssh key for GitLab
ssh-keygen -t rsa -b 4096 -f ~/.ssh/id_rsa -N '' -C $email
# upload ~/.ssh/id_rsa.pub to GitLab

# git setup
git config --global user.name $name
git config --global user.email $email

# install node
curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash -
sudo apt-get install -y nodejs
